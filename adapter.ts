import {Contact} from "./types";

export default class AnonymOrder implements Contact {

    public fullname: string;
    public email: string;
    public phone: string;
    public address: string;
    public postalCode: string;
    public city: string;

    constructor(){
        this.fullname = "******"
        this.email = "******"
        this.phone = "******"
        this.address = "******"
        this.postalCode= "******"
        this.city = "******"
    }
}
