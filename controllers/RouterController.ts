import storage from "node-persist";
import {Request, Response} from "express";
import Order from "types";
import AnonymOrder from "../adapter";


export class RouterController {

    static async getOrders(req: Request, res: Response) {
        const orders: Order[] = await storage.getItem('orders')

        const anonymeOrder = orders.map(order =>
            new AnonymOrder()
            )
            res.json(anonymeOrder)

    }

    static async getOrderByID(req: Request, res: Response) {
        const id = req.params.id

        const orders = await storage.getItem('orders')
        const result = orders.find((order: any) => order.id === parseInt(id, 10))

        if (!result) {
            return res.sendStatus(404)
        }

        return res.json(result)
    }

    static async postOrder(req: Request, res: Response) {
        const payload = req.body

        const orders = await storage.getItem('orders')
        const alreadyExists = orders.find((order: any) => order.id === payload.id)

        if (alreadyExists) {
            return res.sendStatus(409)
        }

        orders.push(payload)

        await storage.setItem('orders', orders)

        res.sendStatus(201)
    }

}
