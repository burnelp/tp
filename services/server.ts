import app from "./app";
import * as http from "http";
import {Express} from "express";

export default class Server {

    public port: number;
    public app: Express;

    constructor(port: number) {
        this.port = this.normalizePort(process.env.PORT || port);
        this.app = app.getInstance(port)
        this.start();
    }

    public start() {
        const server = http.createServer(this.app)
        server.listen(this.port)
        server.on('error', this.onError)
        server.on('listening', () => {
            this.onListening.apply(this)
        })
    }

    protected normalizePort(value: any): number {
        const port = parseInt(value, 10)

        if (isNaN(port)) {
            return value
        }

        if (port >= 0) {
            return port
        }

        return 0
    }

    public onListening() {
        console.log(`Listening on ${this.port}`)
    }

    protected onError(error: any): never {
        if (error.syscall !== 'listen') {
            throw error
        }

        switch (error.code) {
            case 'EACCES':
                console.error(`${this.port} requires elevated privileges`)
                process.exit(1)
            case 'EADDRINUSE':
                console.error(`${this.port} is already in use`)
                process.exit(1)
            default:
                throw error
        }
    }
}
