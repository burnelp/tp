import {Request, Response, Router} from 'express'
import {IndexController} from "../controllers/IndexController";

const router = Router()

router.get('/', async (req: Request, res: Response) => {
  await IndexController.defaultCall(req, res)})

  router.get('/favicon.ico', (req: Request, res: Response) => {
    IndexController.getFavIco(req, res)
})

export default router