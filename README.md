1 - J'ai créé deux fichiers (app.ts et server.ts) dans un dossier services, et dans mon fichier www.ts j'ai juste à créer le server

2 - Le pattern d'architecture qui me semble le plus adapté est le modèle vue controller (mvc)

3 - Ce type respecte le principe I du SOLID (Interface segregation principle)

4 - Pour cette partie j'ai utilisé un adapter. J'ai choisi de ne réécrire que la partie contact en caché, car la facon dont j'ai codé cette question m'aurait obligé à tout réécrire dans les constructeurs, mais j'ai compris le principe :) (j'espere)

6 - Pour cette question, j'ai utilisé un magnifique module qui s'appelle classdiagram-ts <3 